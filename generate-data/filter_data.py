#!/usr/bin/env python3
"""Filter out and clean the data in RBI data."""


def state(state):
    """Filter and clean the states to valid options.

    Keyword arguments:
    state   --  the value to filter
    """
    state = state.lower().replace(' ', '')
    if state == 'andhrapradesh' or state == 'ap':
        return 'andhra pradesh'
    if state == 'arunachalpradesh':
        return 'arunachal pradesh'
    if state == 'assam':
        return 'assam'
    if state == 'bihar':
        return 'bihar'
    if state == 'chattisgarh' or state == 'chhatisgarh' or state == 'chhatishgarh' or state == 'chhattisgarh' or state == 'jhagrakhandcolliery' or state == 'cg':
        return 'chhattisgarh'
    if state == 'goa':
         return 'goa'
    if state == 'gujarat' or state == 'gujurat' or state == 'gujrat':
        return 'gujarat'
    if state == 'haryana':
        return 'haryana'
    if state == 'himachalpradesh' or state == 'himanchalpradesh':
        return 'himachal pradesh'
    if state == 'jharkhand' or state == 'harkhand':
        return 'jharkhand'
    if state == 'karnataka' or state == 'karanataka' or state == 'ka':
        return 'karnataka'
    if state == 'kerela' or state == 'kerala':
        return 'kerala'
    if state == 'madhyapradesh':
        return 'madhya pradesh'
    if state == 'maharashtra' or state == 'maharastra' or state == 'mumbai' or state == 'mh':
        return 'maharashtra'
    if state == 'manipur':
        return 'manipur'
    if state == 'meghalaya':
        return 'meghalaya'
    if state == 'mizoram':
        return 'mizoram'
    if state == 'nagaland':
        return 'nagaland'
    if state == 'odisha' or state == 'orissa':
        return 'odisha'
    if state == 'punjab' or state == 'panjab':
        return 'punjab'
    if state == 'rajasthan' or state == 'rajastan' or state == 'rj':
        return 'rajasthan'
    if state == 'sikkim':
        return 'sikkim'
    if state == 'tamilnadu' or state == 'tn':
        return 'tamil nadu'
    if state == 'telengana' or state == 'telangana':
        return 'telangana'
    if state == 'tripura':
        return 'tripura'
    if state == 'uttarpradesh':
        return 'uttar pradesh'
    if state == 'uttarakand' or state == 'uttarakhand' or state == 'uttrakhand' or state == 'uttaranchal':
        return 'uttarakhand'
    if state == 'westbengal' or state == 'vpo-khandwapattachuru,distchuru':
        return 'west bengal'
    if state == 'andamanandnicobarisland' or state == 'andamanandnicobarislands' or state == 'andamanandnicobar':
        return 'andaman & nicobar islands'
    if state == 'chandigarh' or state == 'chandigarhut':
        return 'chandigarh'
    if state == 'dadraandnagarhavelianddamananddiu' or state == 'dadraandnagarhaveli' or state == 'dadra' or state == 'daman' or state == 'damananddiu':
        return 'dadra & nagar haveli & daman & diu'
    if state == 'delhi' or state == 'newdelhi':
        return 'delhi'
    if state == 'jammuandkashmir':
        return 'jammu & kashmir'
    if state == 'ladakh':
        return 'ladakh'
    if state == 'lakshadweep':
        return 'lakshadweep'
    if state == 'puducherry' or state == 'pondicherry':
        return 'puducherry'

def print_func():
    """Simply prints the purpose of function."""
    print('this program filters the data collected by the rbi for valid results')

if '__name__' == '__main__':
    print_func()
