#!/usr/bin/env python3
"""Convert the banks list excel sheet to sqlite database."""

import sqlite3
from sqlite3 import Error
import pandas as pd
import filter_data as fd


def create_connection(db_file):
    """Create and return a sqlite db connection.

    Keyword arguments:
    db_file --  database file name
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as err:
        print(err)
    return conn


def create_table(conn, create_table_sql):
    """Create a table from the create_table_sql statement.

    Keyword arguments:
    conn                --  db connection object
    create_table_sql    --  sql statement to create table
    """
    try:
        cur = conn.cursor()
        cur.execute(create_table_sql)
    except Error as err:
        print(err)


def generate_insert_statements():
    """Return a list of insert statements."""
    insert = {}
    insert['bank'] = "INSERT INTO banks(name) VALUES(?)"
    insert['state'] = "INSERT INTO states(name) VALUES(?)"
    insert['district'] = "INSERT INTO districts(name,state_id) VALUES(?,?)"
    insert['city'] = "INSERT INTO cities(name,district_id) VALUES(?,?)"
    insert['branch'] = """
    INSERT INTO branches(name,ifsc,address,city_id,bank_id)
    VALUES(?,?,?,?,?)
    """
    return insert

def generate_select_statements():
    """Return a list of select statements."""
    select = {}
    select['bank'] = "SELECT * FROM banks WHERE name=?"
    select['state'] = "SELECT * FROM states WHERE name=?"
    select['district'] = "SELECT * FROM districts WHERE name=? AND state_id=?"
    select['city'] = "SELECT * FROM cities WHERE name=? AND district_id=?"
    select['branch'] = "SELECT * FROM branches WHERE ifsc=?"
    return select


def generate_table_statements():
    """Return a list of table create statements."""
    tables = {}
    tables['banks'] = """
    CREATE TABLE IF NOT EXISTS banks (
        id integer PRIMARY KEY,
        name text NOT NULL
    );
    """
    tables['states'] = """
    CREATE TABLE IF NOT EXISTS states (
        id integer PRIMARY KEY,
        name text NOT NULL
    );
    """
    tables['districts'] = """
    CREATE TABLE IF NOT EXISTS districts (
        id integer PRIMARY KEY,
        name text NOT NULL,
        state_id integer NOT NULL,
        FOREIGN KEY (state_id) REFERENCES states (id)
    );
    """
    tables['cities'] = """
    CREATE TABLE IF NOT EXISTS cities (
        id integer PRIMARY KEY,
        name text NOT NULL,
        district_id integer NOT NULL,
        FOREIGN KEY (district_id) REFERENCES districts (id)
    );
    """
    tables['branches'] = """
    CREATE TABLE IF NOT EXISTS branches (
        id integer PRIMARY KEY,
        name text NOT NULL,
        ifsc text NOT NULL,
        address text NOT NULL,
        city_id integer NOT NULL,
        bank_id integer NOT NULL,
        FOREIGN KEY (city_id) REFERENCES cities (id),
        FOREIGN KEY (bank_id) REFERENCES banks (id)
    );
    """
    return tables


def insert_row(conn, sql, data):
    """Insert data to the database table and return the lastrowid.

    Keyword arguments:
    conn    --  db connection object
    sql     --  sql statement for insert
    data    --  data to insert
    """
    cur = conn.cursor()
    cur.execute(sql, data)
    conn.commit()


def generate_tables(conn):
    """Create tables from the statements.

    Keyword arguments:
    conn                --  db connection object
    """
    statements = generate_table_statements()
    for statement in statements.values():
        #print(statement)
        create_table(conn, statement)


def convert_to_db(conn):
    """Read the xlsx and convert to db."""
    xlsx = pd.ExcelFile('banks.xlsx')
    insert = generate_insert_statements()
    select = generate_select_statements()
    i = 1;
    for sheet in xlsx.sheet_names:
        '''
        only for testing
        if(sheet == 'Sheet1'):
            continue
        '''
        data = xlsx.parse(sheet_name=sheet).fillna('unavailable')
        cur = conn.cursor()
        for row in data.itertuples():
            print(f" row {i}")
            i += 1
            # check if branch exists
            ifsc = str(row[2]).strip()
            data = (ifsc, )
            branch = cur.execute(select['branch'], data).fetchone()
            if branch:
                continue
            # bank
            bank_name = str(row[1]).strip().upper()
            data = (bank_name,)
            bank = cur.execute(select['bank'], data).fetchone()
            if not bank:
                insert_row(conn, insert['bank'], data)
                bank = cur.execute(select['bank'], data).fetchone()
            # state
            state_name = fd.state(row[10]).title()
            data = (state_name,)
            state = cur.execute(select['state'], data).fetchone()
            if not state:
                insert_row(conn, insert['state'], data)
                state = cur.execute(select['state'], data).fetchone()
            # district
            district_name = str(row[9]).strip().title()
            data = (district_name, state[0], )
            district = cur.execute(select['district'], data).fetchone()
            if not district:
                insert_row(conn, insert['district'], data)
                district = cur.execute(select['district'], data).fetchone()
            # city
            city_name = str(row[8]).strip().title()
            data = (city_name, district[0], )
            city = cur.execute(select['city'], data).fetchone()
            if not city:
                insert_row(conn, insert['city'], data)
                city = cur.execute(select['city'], data).fetchone()
            # branch
            branch_name = str(row[4]).strip().title()
            address = str(row[5]).strip()
            data = (ifsc, branch_name, address, city[0], bank[0], )
            insert_row(conn, insert['branch'], data)


def start():
    """Start point of the program."""
    conn = create_connection('./database.db')
    if conn is None:
        print("Error! cannot connect to database.")
        return
    generate_tables(conn)
    convert_to_db(conn)
    conn.close()


if __name__ == '__main__':
    start()
