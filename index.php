<?php

$uri = explode('?', $_SERVER['REQUEST_URI'], 2)[0];

require_once 'functions.php';
if(preg_match('/^\/$/', $uri))
  require_once 'home.php';
else if(preg_match('/^\/state\/([0-9]+)\/districts\/?$/i', $uri, $vars))
  getDistricts($vars[1]);
else if(preg_match('/^\/district\/([0-9]+)\/cities\/?$/i', $uri, $vars))
  getCities($vars[1]);
else if(preg_match('/^\/city\/([0-9]+)\/bank\/([0-9]+)\/branches\/?$/i', $uri, $vars))
  getBranches($vars[1], $vars[2]);
else
  require_once '404.php';
