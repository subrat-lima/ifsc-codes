<!DOCTYPE html>
<html lang="en">
<head>
  <title>Page Not Found</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  width: 100%;
}
h1 {
margin: 4rem 0;
text-align: center;
}
</style>
</head>
<body>
  <h1>The page you are looking for doesn't exist.</h1>
</body>
</html>
