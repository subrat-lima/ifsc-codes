# IFSC Codes

Its a simple web application to fetch the Bank IFSC codes of a locality built using PHP, HTML, CSS, JS and SQLite.
I have used python to convert the spreadsheet to sqlite database.
Data source : [RBI Site](https://www.rbi.org.in/Scripts/neftIFS.aspx)

[Live application demo link](https://ifsc.subratlima.xyz/)

## TODO:
* Tidy up code
* UI improvements
* Clean database (maybe later depending on usage)
