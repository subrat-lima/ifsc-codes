const bank = document.getElementById('bank');
const state = document.getElementById('state');
const district = document.getElementById('district');
const city = document.getElementById('city');
const output = document.getElementById('output');

const districtBlock = document.getElementById('div-district');
const cityBlock = document.getElementById('div-city');

const btn = document.getElementById('btn');

const setBranch = (row) => {
  const elem = document.createElement('article');
  elem.innerHTML = `
    <p class="ifsc">IFSC : ${row.ifsc}</p>
    <p class="branch">Branch : ${row.name}</p>
    <p class="address">Address : ${row.addr}</p>
  `;
  return elem;
};

const setOption = (row) => {
  const opt = document.createElement('option');
  opt.setAttribute('value', row['id']);
  opt.innerHTML = row['name'];
  return opt;
}

const setOptions = (url, elem) => {
  fetch(url)
    .then(res => res.json())
    .then(data => {
      data.forEach(row => elem.appendChild(setOption(row)))
    })
};

const getDistricts = () => {
  resetAll();
  url = `/state/${state.value}/districts`;
  setOptions(url, district);
  districtBlock.classList.remove('hide');
};

const getCities = () => {
  resetCityBlock();
  url = `/district/${district.value}/cities`;
  setOptions(url, city);
  cityBlock.classList.remove('hide');
  btn.removeAttribute('disabled');
}

const getBranches = () => {
  output.innerHTML = '';
  url = `/city/${city.value}/bank/${bank.value}/branches`;
  fetch(url)
    .then(res => res.json())
    .then(data => {
      data.forEach(row => output.appendChild(setBranch(row)))
    })
  output.classList.remove('hide');
}

const resetAll = () => {
  district.innerHTML = '';
  districtBlock.classList.add('hide');
  resetCityBlock();
}

const resetCityBlock = () => {
  btn.setAttribute('disabled', 'true');
  city.innerHTML = '';
  cityBlock.classList.add('hide');
  resetOutputBlock();
}

const resetOutputBlock = () => {
  output.innerHTML = '';
  output.classList.add('hide');
};
