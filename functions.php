<?php
$db = new PDO('sqlite:database.db');

function getRows($column) {
  global $db;
  $rows = $db->query("SELECT * FROM ".$column);
  print "<option>-- Select --</option>";
  foreach($rows as $row)
    print "<option value='".$row['id']."'>".$row['name']."</option>";
}

function getData($query) {
  global $db;
  $rows = $db->query($query);
  return $rows;
}

function getDistricts($state_id) {
  $query = "SELECT * FROM districts where state_id=".$state_id;
  $rows = getData($query);
  $districts = array();
  foreach($rows as $row) {
    $district = array('id' => $row['id'], 'name' => $row['name']);
    array_push($districts, $district);
  }
  sendData($districts);
}

function getCities($district_id) {
  $query = "SELECT * FROM cities where district_id=".$district_id;
  $rows = getData($query);
  $cities = array();
  foreach($rows as $row) {
    $city = array('id' => $row['id'], 'name' => $row['name']);
    array_push($cities, $city);
  }
  sendData($cities);
}

function getBranches($city_id, $bank_id) {
  $query = "SELECT * FROM branches WHERE city_id=".$city_id." AND bank_id=".$bank_id;
  $rows = getData($query);
  $branches = array();
  foreach($rows as $row) {
    $branch = array('ifsc' => $row['ifsc'], 'name' => $row['name'], 'addr' => $row['address']);
    array_push($branches, $branch);
  }
  sendData($branches);
}


function sendData($data) {
  header('Content-Type: application/json');
  echo json_encode($data);
}
?>
