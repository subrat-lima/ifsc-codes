<!DOCTYPE html>
<html lang="en">
<head>
  <title>IFSC Codes</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <h1>IFSC Codes</h1>
  <main>
    <section class="inputs">
      <div id="div-bank" class="option">
        <label for="bank">Bank</label>
        <select id="bank">
          <?php getRows('banks') ?>
        </select>
      </div>
      <div id="div-state" class="option">
        <label for="state">State</label>
        <select id="state" onchange="getDistricts()">
          <?php getRows('states') ?>
        </select>
      </div>
      <div id="div-district" class="option hide">
        <label for="district">District</label>
        <select id="district" onchange="getCities()"></select>
      </div>
      <div id="div-city" class="option hide">
        <label for="city">City</label>
        <select id="city" onchange="resetOutputBlock()"></select>
      </div>
      <div id="div-buttons">
      <button id="btn" class="button" onclick="getBranches()" disabled>Search</button>
      <button class="button" onclick="resetAll()">Reset</button>
      </div>
    </section>
    <section id="output" class="hide">
    </section>
  </main>
  <script src="app.js"></script>
</body>
</html>
